<?php
/**
 * Cimbre - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Cimbre
 * @author    Cimbre <contato@cimbre.com.br>
 * @copyright 2019 Cimbre
 * @license   Proprietary https://cimbre.com.br
 * @link      https://cimbre.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Cimbre - mu-plugin
 * Plugin URI:  https://cimbre.com.br
 * Description: Customizations for cimbre.com.br site
 * Version:     1.0.0.1
 * Author:      Cimbre
 * Author URI:  https://cimbre.com.br/
 * Text Domain: cimbrecombr
 * License:     Proprietary
 * License URI: https://cimbre.com.br
 */

 // If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 */
function cimbrecombr_load_textdomain() 
{
    load_muplugin_textdomain('cimbrecombr', basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'cimbrecombr_load_textdomain');

/**
 *
 * Custom Post Types
 *
 */

/**
 * Works
 */
function cpt_cimbre_works() 
{
    $labels = array(
            'name'                  => __('Works', 'cimbrecombr'),
            'singular_name'         => __('Work', 'cimbrecombr'),
            'menu_name'             => __('Works', 'cimbrecombr'),
            'name_admin_bar'        => __('Work', 'cimbrecombr'),
            'archives'              => __('Work Archives', 'cimbrecombr'),
            'attributes'            => __('Work Attributes', 'cimbrecombr'),
            'parent_item_colon'     => __('Parent Work:', 'cimbrecombr'),
            'all_items'             => __('All Works', 'cimbrecombr'),
            'add_new_item'          => __('Add New Work', 'cimbrecombr'),
            'add_new'               => __('Add work', 'cimbrecombr'),
            'new_item'              => __('New Work', 'cimbrecombr'),
            'edit_item'             => __('Edit Work', 'cimbrecombr'),
            'update_item'           => __('Update Work', 'cimbrecombr'),
            'view_item'             => __('View Work', 'cimbrecombr'),
            'view_items'            => __('View Works', 'cimbrecombr'),
            'search_items'          => __('Search Work', 'cimbrecombr'),
            'not_found'             => __('Not found', 'cimbrecombr'),
            'not_found_in_trash'    => __('Not found in Trash', 'cimbrecombr'),
            'featured_image'        => __('Featured Image', 'cimbrecombr'),
            'set_featured_image'    => __('Set featured image', 'cimbrecombr'),
            'remove_featured_image' => __('Remove featured image', 'cimbrecombr'),
            'use_featured_image'    => __('Use as featured image', 'cimbrecombr'),
            'insert_into_item'      => __('Insert into work', 'cimbrecombr'),
            'uploaded_to_this_item' => __('Uploaded to this work', 'cimbrecombr'),
            'items_list'            => __('Works list', 'cimbrecombr'),
            'items_list_navigation' => __('Works list navigation', 'cimbrecombr'),
            'filter_items_list'     => __('Filter Works list', 'cimbrecombr'),
    );
    $rewrite = array(
            'slug'                  => __('works', 'cimbrecombr'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Work', 'cimbrecombr'),
            'description'           => __('Cimbre Works', 'cimbrecombr'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array('category', 'post_tag'),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 7,
            'menu_icon'             => 'dashicons-admin-page',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('cimbre_works', $args); 
} 
add_action('init', 'cpt_cimbre_works', 0);

/**
 * Works Custom Fields
 */
function cmb_cimbre_work() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_work_';
    
    /**
    * Initiate the metabox
    */
    $cmb_works = new_cmb2_box(
        array(
            'id'            => 'cimbrecombr_works',
            'title'         => __('Work', 'cimbrecombr'),
            'object_types'  => array('cimbre_works'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Description
    $cmb_works->add_field( 
        array(
            'name'       => __('Description', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'text',
            'type'       => 'textarea_code',
        )
    );

    //Local
    $cmb_works->add_field( 
        array(
            'name'       => __('Client', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'client',
            'type'       => 'text',
        )
    );

    //Date
    $cmb_works->add_field( 
        array (
            'name'       => __('Date', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'date',
            'type' => 'text',
        ) 
    );

    //Featured Video
    $cmb_works->add_field( 
        array (
            'name' => __('Featured Video', 'cimbrecombr'),
            'desc' => __('Enter a youtube or vimeo URL.', 'cimbrecombr'),
            'id'   => $prefix . 'featured_video',
            'type' => 'oembed',
        ) 
    );

    //Featured Image
    $cmb_works->add_field(
        array (
            'name'        => __('Featured Image', 'cimbrecombr'),
            'description' => '',
            'id'          => $prefix . 'featured_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Gallery Title
    $cmb_works->add_field( 
        array(
        'name' => __('Gallery Title', 'cimbrecombr'),
        'desc' => '',
        'type' => 'textarea_code',
        'id'   => $prefix.'gallery_title',
        )
    );

    //Gallery Text
    $cmb_works->add_field( 
        array(
        'name' => __('Gallery Text', 'cimbrecombr'),
        'desc' => '',
        'id' => $prefix.'gallery_text',
        'type' => 'textarea_code',
        ) 
    );

    //Gallery Images
    $cmb_works->add_field(
        array(
            'name' => __('Images', 'cimbrecombr'),
            'id'   => $prefix.'gallery_images',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'cimbrecombr'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'cimbrecombr'), // default: "Remove Image"
                'file_text' => __('File', 'cimbrecombr'), // default: "File:"
                'file_download_text' => __('Download', 'cimbrecombr'), // default: "Download"
                'remove_text' => __('Remove', 'cimbrecombr'),
                'use_text' => __('Remove', 'cimbrecombr'), // default: "Remove"
                'upload_file'  => __('Use this file', 'cimbrecombr'),
                'upload_files' => __('Use these files', 'cimbrecombr'),
                'remove_image' => __('Remove Image', 'cimbrecombr'),
                'remove_file'  => __('Remove', 'cimbrecombr'),
                'file'         => __('File:', 'cimbrecombr'),
                'download'     => __('Download', 'cimbrecombr'),
                'check_toggle' => __('Select / Deselect All', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );
}
add_action('cmb2_admin_init', 'cmb_cimbre_work');

/**
 * Solutions
 */
function cpt_cimbre_solutions() 
{
    $labels = array(
            'name'                  => __('Solutions', 'cimbrecombr'),
            'singular_name'         => __('Solution', 'cimbrecombr'),
            'menu_name'             => __('Solutions', 'cimbrecombr'),
            'name_admin_bar'        => __('Solution', 'cimbrecombr'),
            'archives'              => __('Solution Archives', 'cimbrecombr'),
            'attributes'            => __('Solution Attributes', 'cimbrecombr'),
            'parent_item_colon'     => __('Parent Solution:', 'cimbrecombr'),
            'all_items'             => __('All Solutions', 'cimbrecombr'),
            'add_new_item'          => __('Add New Solution', 'cimbrecombr'),
            'add_new'               => __('Add solution', 'cimbrecombr'),
            'new_item'              => __('New Solution', 'cimbrecombr'),
            'edit_item'             => __('Edit Solution', 'cimbrecombr'),
            'update_item'           => __('Update Solution', 'cimbrecombr'),
            'view_item'             => __('View Solution', 'cimbrecombr'),
            'view_items'            => __('View Solutions', 'cimbrecombr'),
            'search_items'          => __('Search Solution', 'cimbrecombr'),
            'not_found'             => __('Not found', 'cimbrecombr'),
            'not_found_in_trash'    => __('Not found in Trash', 'cimbrecombr'),
            'featured_image'        => __('Featured Image', 'cimbrecombr'),
            'set_featured_image'    => __('Set featured image', 'cimbrecombr'),
            'remove_featured_image' => __('Remove featured image', 'cimbrecombr'),
            'use_featured_image'    => __('Use as featured image', 'cimbrecombr'),
            'insert_into_item'      => __('Insert into solution', 'cimbrecombr'),
            'uploaded_to_this_item' => __('Uploaded to this solution', 'cimbrecombr'),
            'items_list'            => __('Solutions list', 'cimbrecombr'),
            'items_list_navigation' => __('Solutions list navigation', 'cimbrecombr'),
            'filter_items_list'     => __('Filter Solutions list', 'cimbrecombr'),
    );
    $rewrite = array(
            'slug'                  => __('solutions', 'cimbrecombr'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Solution', 'cimbrecombr'),
            'description'           => __('Cimbre Solutions', 'cimbrecombr'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 8,
            'menu_icon'             => 'dashicons-admin-page',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('cimbre_solutions', $args); 
} 
add_action('init', 'cpt_cimbre_solutions', 0);

/**
 * Solutions Custom Fields
 */
function cmb_cimbre_solution() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_solution_';
    
    /**
    * Initiate the metabox
    */
    $cmb_home = new_cmb2_box(
        array(
            'id'            => 'cimbrecombr_solutions',
            'title'         => __('Home', 'cimbrecombr'),
            'object_types'  => array('cimbre_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );
    
    //Home Image
    $cmb_home->add_field( 
        array(
            'name'        => __('Image in Home', 'cimbrecombr'),
            'description' => '',
            'id'          => $prefix . 'home_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Home Text
    $cmb_home->add_field( 
        array(
            'name'       => __('Text in Home', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'home_text',
            'type'       => 'textarea_code',
        )
    );

    /**
    * Initiate the metabox
    */
    $cmb_solutions = new_cmb2_box(
        array(
            'id'            => 'cimbrecombr_solutions',
            'title'         => __('Solution Classes', 'cimbrecombr'),
            'object_types'  => array('cimbre_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Classes Group
    $solutions_id = $cmb_solutions->add_field( 
        array(
            'id'          => $prefix . 'classes',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Class {#}', 'cimbrecombr'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Class', 'cimbrecombr'),
                'remove_button' => __('Remove Class', 'cimbrecombr'),
                'sortable'      => true, // beta
            ),
        )
    );
    
    //Class Image
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name'        => __('Class Image', 'cimbrecombr'),
            'description' => '',
            'id'          => 'class_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Picture', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Class Name
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name' => __('Class Name', 'cimbrecombr'),
            'id'   => 'class_name',
            'type' => 'text',
        ) 
    );

    //Class Description
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
        'name' => __('Class Description', 'cimbrecombr'),
        'id'   => 'class_desc',
        'description' => '',
        'type' => 'textarea_code',
        ) 
    );

    /**
    * Initiate the metabox
    */
    $cmb_solution = new_cmb2_box(
        array(
            'id'            => 'cimbrecombr_solutions',
            'title'         => __('Solution', 'cimbrecombr'),
            'object_types'  => array('cimbre_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Section Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Solution Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'solution_title',
            'type'       => 'textarea_code',
        )
    );

    //Description Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Description Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'desc_title',
            'type'       => 'textarea_code',
        )
    );

    //Description
    $cmb_solution->add_field( 
        array(
            'name'       => __('Description', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'text',
            'type'       => 'textarea_code',
        )
    );

    //Portfolio Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Portfolio Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'portfolio_title',
            'type'       => 'textarea_code',
        )
    );
    
    //Catchphrase
    $cmb_solution->add_field( 
        array(
            'name'       => __('Catchphrase', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'catchphrase',
            'type'       => 'text',
        )
    );

    //Catchphrase Author
    $cmb_solution->add_field( 
        array(
            'name'       => __('Catchphrase Author', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'catchphrase_author',
            'type'       => 'text',
        )
    );

    //Contato CTA
    $cmb_solution->add_field( 
        array(
            'name'       => __('Contact CTA', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_cimbre_solution');

/**************************
 * Front-page Custom Fields
 **************************/
function cmb_cimbre_frontpage()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_frontpage_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'frontpage_hero_id',
            'title'         => __('Hero', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'textarea_code',
        )
    );

    //Hero Text
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Text', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * About
     ******/
    $cmb_about = new_cmb2_box(
        array(
            'id'            => 'frontpage_about_id',
            'title'         => __('About', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //About Title
    $cmb_about->add_field( 
        array(
            'name'       => __('About Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'about_title',
            'type'       => 'textarea_code',
        )
    );

    //About Text
    $cmb_about->add_field( 
        array(
            'name'       => __('About Text', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'about_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Portfolio
     ******/
    $cmb_portfolio = new_cmb2_box(
        array(
            'id'            => 'frontpage_portfolio_id',
            'title'         => __('Portfolio', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Portfolio Title
    $cmb_portfolio->add_field( 
        array(
            'name'       => __('Portfolio Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'portfolio_title',
            'type'       => 'textarea_code',
        )
    );

    /******
     * News
     ******/
    $cmb_news = new_cmb2_box(
        array(
            'id'            => 'frontpage_news_id',
            'title'         => __('News', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //News Title
    $cmb_news->add_field( 
        array(
            'name'       => __('News Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'news_title',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Contact
     ******/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'frontpage_contact_id',
            'title'         => __('Contact', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'contact_title',
            'type'       => 'textarea_code',
        )
    );

    //Contact CTA
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact CTA', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Footer
     ******/
    $cmb_footer = new_cmb2_box(
        array(
            'id'            => 'frontpage_footer_id',
            'title'         => __('Footer', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Footer Text
    $cmb_footer->add_field( 
        array(
            'name'       => __('Footer Text', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'footer_text',
            'type'       => 'textarea_code',
        )
    );

    //Footer Contact
    $cmb_footer->add_field( 
        array(
            'name'       => __('Footer Contact', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'footer_contact',
            'type'       => 'title',
        )
    );

    //Footer Address
    $cmb_footer->add_field( 
        array(
            'name'       => __('Address', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'footer_address',
            'type'       => 'textarea_code',
        )
    );

    //Footer Phone
    $cmb_footer->add_field( 
        array(
            'name'       => __('Phones', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'footer_phones',
            'type'       => 'textarea_code',
        )
    );

    //Footer E-mail
    $cmb_footer->add_field( 
        array(
            'name'       => __('E-mail', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'footer_email',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_cimbre_frontpage');

/**************************
 * About Page Custom Fields
 **************************/
function cmb_cimbre_about()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_about_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'about_hero_id',
            'title'         => __('Hero', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

    /**********
     * Services
     **********/
    $cmb_services = new_cmb2_box(
        array(
            'id'            => 'about_services_id',
            'title'         => __('Services', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Services Title
    $cmb_services->add_field( 
        array(
            'name'       => __('Services Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'services_title',
            'type'       => 'textarea_code',
        )
    );

    //Services Group
    $services_id = $cmb_services->add_field( 
        array(
            'id'          => $prefix.'services',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Service {#}', 'cimbrecombr'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Service', 'cimbrecombr'),
                'remove_button' => __('Remove Service', 'cimbrecombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Service Title
    $cmb_services->add_group_field( 
        $services_id, array(
            'name' => __('Title', 'cimbrecombr'),
            'id'   => 'title',
            'type' => 'text',
        ) 
    );

    //Service Description
    $cmb_services->add_group_field( 
        $services_id, array(
        'name' => __('Description', 'cimbrecombr'),
        'id'   => 'desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );
    
    /*********
     * Company
     *********/
    $cmb_company = new_cmb2_box(
        array(
            'id'            => 'about_company_id',
            'title'         => __('Company', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Featured Video
    $cmb_company->add_field( 
        array (
            'name' => __('Featured Video', 'cimbrecombr'),
            'desc' => __('Enter a youtube or vimeo URL.', 'cimbrecombr'),
            'id'   => $prefix . 'company_featured_video',
            'type' => 'oembed',
        ) 
    );

    //Featured Image
    $cmb_company->add_field(
        array (
            'name'        => __('Featured Image', 'cimbrecombr'),
            'description' => '',
            'id'          => $prefix . 'company_featured_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Company Title
    $cmb_company->add_field( 
        array(
            'name'       => __('Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'company_title',
            'type'       => 'textarea_code',
        )
    );

    //Company Text
    $cmb_company->add_field( 
        array(
            'name'       => __('Text', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'company_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Team
     ******/
    $cmb_team = new_cmb2_box(
        array(
            'id'            => 'about_team_id',
            'title'         => __('Team', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Team Title
    $cmb_company->add_field( 
        array(
            'name'       => __('Team Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'team_title',
            'type'       => 'textarea_code',
        )
    );
    
    //Team Group
    $team_id = $cmb_team->add_field( 
        array(
            'id'          => $prefix.'team',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Team Member {#}', 'cimbrecombr'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Team Member', 'cimbrecombr'),
                'remove_button' => __('Remove Team Member', 'cimbrecombr'),
                'sortable'      => true, // beta
            ),
        )
    );
    
    //Member Image
    $cmb_team->add_group_field( 
        $team_id, array(
            'name'        => __('Profile Picture', 'cimbrecombr'),
            'description' => '',
            'id'          => 'member_picture',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Picture', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Member Name
    $cmb_team->add_group_field( 
        $team_id, array(
            'name' => __('Name', 'cimbrecombr'),
            'id'   => 'member_name',
            'type' => 'text',
        ) 
    );

    //Member Occupation
    $cmb_team->add_group_field( 
        $team_id, array(
        'name' => __('Occupation', 'cimbrecombr'),
        'id'   => 'member_occupation',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    //Member Desc
    $cmb_team->add_group_field( 
        $team_id, array(
        'name' => __('Description', 'cimbrecombr'),
        'id'   => 'member_desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    /****************
     * Testimonials
     ***************/
    $cmb_testimonials = new_cmb2_box(
        array(
            'id'            => 'about_testimonials_id',
            'title'         => __('Testimonials', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Testimonials Group
    $testimonials_id = $cmb_testimonials->add_field( 
        array(
            'id'          => $prefix.'testimonials',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Testimonial {#}', 'cimbrecombr'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Entry', 'cimbrecombr'),
                'remove_button' => __('Remove Entry', 'cimbrecombr'),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        )
    );

    //Testimonial Desc
    $cmb_testimonials->add_group_field( 
        $testimonials_id, array(
        'name' => __('Testimonial', 'cimbrecombr'),
        'id'   => 'testimonial',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    //Testimonial Author
    $cmb_testimonials->add_group_field( 
        $testimonials_id, array(
        'name' => __('Author', 'cimbrecombr'),
        'id'   => 'author',
        'description' => '',
        'type' => 'text',
        ) 
    );
    
    /******
     * Contact
     ******/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'about_contact_id',
            'title'         => __('Contact', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact CTA
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact CTA', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_cimbre_about');

/******************************
 * Solutions Page Custom Fields
 ******************************/
function cmb_cimbre_solutions()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_solutions_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'solutions_hero_id',
            'title'         => __('Hero', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

    /**********
     * Solutions
     **********/
    $cmb_solutions = new_cmb2_box(
        array(
            'id'            => 'solutions_solutions_id',
            'title'         => __('Solutions', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Solutions Title
    $cmb_solutions->add_field( 
        array(
            'name'       => __('Solutions Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'solutions_title',
            'type'       => 'textarea_code',
        )
    );

    //Solutions Group
    $solutions_id = $cmb_solutions->add_field( 
        array(
            'id'          => $prefix.'solutions',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Solution {#}', 'cimbrecombr'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Solution', 'cimbrecombr'),
                'remove_button' => __('Remove Solution', 'cimbrecombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Solution Title
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name' => __('Title', 'cimbrecombr'),
            'id'   => 'title',
            'type' => 'text',
        ) 
    );

    //Solution Description
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
        'name' => __('Description', 'cimbrecombr'),
        'id'   => 'desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    /***************
     * Solution CTA
     **************/
    $cmb_solutions_cta = new_cmb2_box(
        array(
            'id'            => 'solutions_cta_id',
            'title'         => __('Solutions CTA', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Solution CTA
    $cmb_solutions_cta->add_field( 
        array(
            'name'       => __('Solutions CTA', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'cta_text',
            'type'       => 'textarea_code',
        )
    );

    /**********
     * Clients
     **********/
    $cmb_clients = new_cmb2_box(
        array(
            'id'            => 'solutions_clients_id',
            'title'         => __('Clients', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Client Group
    $clients_id = $cmb_clients->add_field( 
        array(
            'id'          => $prefix.'clients',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Client {#}', 'cimbrecombr'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Client', 'cimbrecombr'),
                'remove_button' => __('Remove Client', 'cimbrecombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Client Name
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name' => __('Name', 'cimbrecombr'),
            'id'   => 'name',
            'type' => 'text',
        ) 
    );

    //Client URL
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name' => __('URL', 'cimbrecombr'),
            'id'   => 'url',
            'type' => 'text_url',
        ) 
    );
    
    //Client Logo
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name'        => __('Logo', 'cimbrecombr'),
            'description' => '',
            'id'          => 'logo',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Logo', 'cimbrecombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                ),
            ),
            'preview_size' => array(180, 180)
        )
    );

}
add_action('cmb2_admin_init', 'cmb_cimbre_solutions');

/******************************
 * Blog Page Custom Fields
 ******************************/
function cmb_cimbre_blog()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_blog_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'blog_hero_id',
            'title'         => __('Hero', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            //'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/home.blade.php'),
            'show_on'       =>  array( 'key' => 'home', 'value' => ''),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

}
add_action('cmb2_admin_init', 'cmb_cimbre_blog');

/******************************
 * Contact Page Custom Fields
 ******************************/
function cmb_cimbre_contact()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbrecombr_contact_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'contact_hero_id',
            'title'         => __('Hero', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

    /**********
     * Contact
     **********/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'contact_contact_id',
            'title'         => __('Contact', 'cimbrecombr'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'title',
            'type'       => 'textarea_code',
        )
    );

    //Contact Text
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Text', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'text',
            'type'       => 'textarea_code',
        )
    );

    //Contact Form Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Form Title', 'cimbrecombr'),
            'desc'       => '',
            'id'         => $prefix . 'form_title',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_cimbre_contact');
?>
